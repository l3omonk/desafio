$(document).ready(function ($) {
    responsiveViewClass();
    $(window).resize(function () {
        responsiveViewClass();
    });
    getPage(1);
});

function getPage(itemId) {
    $.getJSON("http://localhost:8888/api/V1/categories/" + itemId, function (data) {
        var domElement = $('#products');
        parseProducts(domElement, data.items);
    })

    $.getJSON("http://localhost:8888/api/V1/categories/list", function (categoryList) {
        // Top menu
        var domElement = $('#menu ul');
        parseMenu(domElement, categoryList.items);

        // Side menu
        domElement = $('#categories ul');
        parseMenu(domElement, categoryList.items);

        // Title
        var domElementTitle = $('#title-page h1');
        parseTitle(domElementTitle, categoryList.items);
    });
}

function parseProducts(element, item) {
    $(element).empty();
    for (var i = 0; i < item.length; i++) {
        var element = $(element).append('<div class="product">' +
            '<p class="thumb"><img src="' + item[i].image + '" class="img-fluid" alt="' + item[i].name + '"></p>' +
            '<p class="description">' + item[i].name + '</p>' +
            '<p class="price">R$ ' + item[i].price + '</p>' +
            '<p><button type="button" class="btn btn-primary btn-lg btn-block">Comprar</button></p>');
    }
}

function parseMenu(listRoot, menu) {
    $(listRoot).empty();
    for (var i = 0; i < menu.length; i++) {
        var li = $(listRoot).append('<li class="nav-item"><a href="#" onclick="getPage(' + menu[i].id + ')">' + menu[i].name + '</a></li>');
    }
}

function parseTitle(h1, title_page) {
    if (itemId == 0) {
        $(h1).empty();
        var h1 = $(h1).append(titlePage[1].name);
    } else {
        $(h1).empty();
        var h1 = $(h1).append(titlePage[itemId].name);
    }

}

$(".navbar-toggler").click(function () {
    $(".main-menu").toggleClass("main-menu-responsive");
    $("main").toggleClass("main-page-responsive");
});

var responsiveViewClass = function () {
    var desktopView = $(document).width();
    if (desktopView >= "768") {
        $("#navbarSupportedContent").removeClass("navbar-collapse collapse")
    } else if (desktopView <= "425") {
        $("#navbarSupportedContent").addClass("navbar-collapse collapse")
    };
    if (desktopView >= "400") {
        $("#bar-init").removeClass("text-center")
    } else if (desktopView <= "401") {
        $("#bar-init").addClass("text-center")
    };
}
